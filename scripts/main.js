// 1.Опишіть своїми словами як працює метод forEach.
//Перебирає і робить перевірку кожного з  елементів масиву 

// 2.Як очистити масив?
//1. встановити його довжину  нуль. Ця операція видаляє всі елементи масиву, і навіть їх значення.
//2.Метод splice() змінює вміст масиву, видаляючи існуючі елементи та/або додаючи нові.

// 3.Як можна перевірити, що та чи інша змінна є масивом?
//використатиметод Array. isArray(). цей метод визначає, чи передану є масивом.Якщо так, цей метод поверне true.


array = ['hello', 'world', 23, '23', null, 35, '35', {}, true, false, [1, 2, 3, 'abcd', 'efgh'], alert, 55, { name: 'Vika', lastName : 'Kishchenko'}, 60];


let filterBy = function (array, type) {
    return array.filter(n => typeof n !== typeof type)
}

console.log(filterBy(array, NaN));
console.log(filterBy(array, 'string'));
console.log(filterBy(array, null));
console.log(filterBy(array, false));
console.log(filterBy(array, function () { }));
console.log(filterBy(array, { }));
